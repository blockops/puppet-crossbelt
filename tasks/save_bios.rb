#!/opt/crossbelt/embedded/bin/ruby
require 'digest'
require 'crossbelt/compute_units'
require 'crossbelt/rig'
require 'net/http'
require 'uri'
require 'json'

def backup_path
  ENV['PT_backup_path']
end

def amd_compute_units
  @amd_compute_units ||= begin
    klasses = [Crossbelt::ComputeUnit::AmdGpu]
    JSON.parse Crossbelt::ComputeUnits.find_only(klasses).to_json
  end
end

def rom_digest(file)
  Digest::SHA1.file file
end

def amdvbflash_path
  path = ENV['PT_amdvbflash_path'] || "/opt/mmp/bin/amdvbflash"
end

def save_rom
  rig_name = Crossbelt::Rig.instance.name
  save_base_path = File.join(backup_path, 'roms', rig_name)
  amd_compute_units.each.with_index do |unit, index|
    FileUtils.mkdir_p(save_base_path) unless File.exist?(save_base_path)
    bios_path = File.join(save_base_path, "#{unit['bios']}_gpu#{index}.rom")
    amd_compute_units[index]['rom_path'] = bios_path
    `#{amdvbflash_path} -s #{index} #{bios_path}`
    if $?.success?
      puts "File saved to #{bios_path}" 
      amd_compute_units[index]['rom_digest'] = rom_digest(bios_path)
    end
  end
  File.write(File.join(save_base_path, 'status.json'), JSON.pretty_generate(amd_compute_units))
end

save_rom