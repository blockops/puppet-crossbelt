#!/opt/crossbelt/embedded/bin/ruby
require 'digest'
require 'crossbelt/compute_units'
require 'crossbelt/rig'
require 'net/http'
require 'uri'
require 'json'

@s3_bucket = ENV['PT_s3_bucket'] || 'crossbelt-roms'
@s3_access_key = ENV['PT_s3_access_key'] 
@s3_secret_key = ENV['PT_s3_secret_key']

def backup_path
  ENV['PT_backup_path']
end

# def upload_to_s3(file_to_upload)
#   upload_path=File.join('/', @s3_bucket, File.basename(File.dirname(file_to_upload)), File.basename(file_to_upload))
#   uri = URI.parse("https://#{@s3_bucket}.s3.amazonaws.com/#{upload_path}")
#   # metadata
#   contentType="application/octet-stream"
#   dateValue=Time.now
#   signature_string="PUT\n\n#{contentType}\n#{dateValue}\n#{upload_path}"
#   sha1 = Digest::SHA1.hexdigest signature_string
#   signature_hash=`echo -en #{signature_string} | openssl sha1 -hmac #{@s3_secret_key} -binary | base64`.chomp

#   require 'pry'; binding.pry
#   request = Net::HTTP::Put.new(uri)
#   request["Host"] = "#{@s3_bucket}.s3.amazonaws.com"
#   request["Date"] = dateValue.to_s
#   request["Content-Type"] = contentType
#   request["Authorization"] = "AWS #{@s3_access_key}:#{signature_hash}"

#   req_options = {
#     use_ssl: uri.scheme == "https"
#   }

#   response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
#     http.request(request)
#   end
#   puts response.code
#   puts response.body
# end

save_rom