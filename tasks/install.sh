#!/usr/bin/env bash
PATH=$PATH:/opt/crossbelt/bin:/opt/crossbelt/embedded/bin
CHANNEL=${PT_channel:-'unstable'}
echo "Installing Crossbelt using repository channel ${CHANNEL}"
CB_FILE="crossbelt-latest-${CHANNEL}.deb"
CB_URL=https://crossbelt.blockops.party/downloads/${CB_FILE}
REMOTE_CB_FILE_SUM=$(curl --head ${CB_URL} -s |grep ETag | cut -d \: -f2)
disk_available=$(df /opt --output=avail -BM | grep M)
wget $CB_URL -O /tmp/${CB_FILE}
sudo apt install -y /tmp/${CB_FILE}
if [[ $? -eq 0 ]]; then
 version_info=$(cb --version)
 echo "Crossbelt Installed successfully"
 rm -f $CB_FILE
else
  exit 1
fi
