#!/usr/bin/env bash
PATH=$PATH:/opt/crossbelt/bin:/opt/crossbelt/embedded/bin
exporters=${PT_exporters:-table,stdout}
cb --exporters=${exporters} cu status