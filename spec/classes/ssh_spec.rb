require "spec_helper"
require "shared_contexts"

describe "crossbelt::ssh" do
  # by default the hiera integration uses hiera data from the shared_contexts.rb file
  # but basically to mock hiera you first need to add a key/value pair
  # to the specific context in the spec/shared_contexts.rb file
  # Note: you can only use a single hiera context per describe/context block
  # rspec-puppet does not allow you to swap out hiera data on a per test block
  #include_context :hiera

  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:extra_facts) do
    {
      cache_bust: Time.now.to_i,
    }
  end

  # let(:hiera_data) do
  #   {
  #     'crossbelt::ssh::authorized_keys' => {
  #       'ethos' => {
  #         'alias' => 'test',
  #         'type'  => 'ssh-rsa',
  #         'key'   => 'fdsafds'
  #       }
  #     }
  #   }
  # end

  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) do
    {
      authorized_keys: {
        "user@example.com" => {
          "type" => "ssh-rsa",
          "key" => "fdsafds",
          "user" => "ethos",
        },
      },
    }
  end
  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)

  on_supported_os(test_on).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      it { is_expected.to compile }

      it do
        is_expected.to contain_ssh_authorized_key("user@example.com").with(
          ensure: "present",
          user: "ethos",
          type: "ssh-rsa",
          key: "fdsafds",
        )
      end
    end
  end
end
