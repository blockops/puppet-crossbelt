require "spec_helper"
require "shared_contexts"

describe "crossbelt::rigs::ethos" do
  # by default the hiera integration uses hiera data from the shared_contexts.rb file
  # but basically to mock hiera you first need to add a key/value pair
  # to the specific context in the spec/shared_contexts.rb file
  # Note: you can only use a single hiera context per describe/context block
  # rspec-puppet does not allow you to swap out hiera data on a per test block
  # include_context :hiera

  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:facts) do
    { gpu_type: "nvidia",
      hostname: "6081e1",
      nvidia_info: [{ "name" => "GeFore GTX 1080",
                      "vbios_version" => "86.04.3B.40.64",
                      "uuid" => "GPU-0c9d8123-fbdd-0371-0dc38fe0ddfd" }],
      uptime_hours: 5 }
  end

  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) do
    {
      dualminer_enabled: true,
      dualminer_currency: "dcr",
      dualminer_pool_name: "suprnova.cc",
      pool_name: "nanopool",
      currency: "etn",
      owner_wallets: { etn: "sdfasdfasdfsdf" },
      miner_attrs: { name: 'xmr-stak' }
    }
  end

  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)
  on_supported_os(test_on).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it do
        is_expected.to contain_file("/home/ethos/remote.conf").with(
          ensure: "present",
          content: "# disabled",
          notify: "Exec[miner_service]",
        )
      end
      it do
        is_expected.to contain_exec("miner_service").with(
          path: ["/opt/ethos/bin", "/usr/bin", "/usr/local/bin"],
          refreshonly: true,
          command: "minestop",
        )
      end
    end
  end
end
