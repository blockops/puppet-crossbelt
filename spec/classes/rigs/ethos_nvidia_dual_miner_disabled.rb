require "spec_helper"
require "shared_contexts"

describe "crossbelt::rigs::ethos" do
  # by default the hiera integration uses hiera data from the shared_contexts.rb file
  # but basically to mock hiera you first need to add a key/value pair
  # to the specific context in the spec/shared_contexts.rb file
  # Note: you can only use a single hiera context per describe/context block
  # rspec-puppet does not allow you to swap out hiera data on a per test block
  # include_context :hiera

  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:facts) do
    {
      gpu_type: "nvidia",
      hostname: "6081e1",
    }
  end

  let(:config_file) { "/home/ethos/local.conf" }

  let(:params) do
    {
      dualminer_enabled: true,
      dualminer_currency: "dcr",
      dualminer_pool_name: "suprnova.cc",
      pool_name: "nanopool",
      owner_wallets: { etn: "sdfasdfasdfsdf" },
      currency: "etn",
      miner_attrs: { name: 'xmr-stak' }

    }
  end

  on_supported_os(test_on).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it { is_expected.to contain_file(config_file).with_content("") }

      it { is_expected.to contain_file(config_file).with_content(%r{dualminer enabled}) }
      it { is_expected.to contain_file(config_file).with_content(%r{dualminer-coin dcr}) }
      it { is_expected.to contain_file(config_file).with_content(%r{dualminer-password x}) }
      it { is_expected.to contain_file(config_file).with_content(%r{dualminer-pool dcr.suprnova.cc:3252}) }
      it { is_expected.to contain_file(config_file).with_content(%r{dualminer-wallet nwps}) }
      it { is_expected.to contain_file(config_file).with_content(%r{dualminer-worker 6081e1}) }
    end
  end
end
