require "spec_helper"
require "shared_contexts"

describe "crossbelt::rigs::ethos" do
  # by default the hiera integration uses hiera data from the shared_contexts.rb file
  # but basically to mock hiera you first need to add a key/value pair
  # to the specific context in the spec/shared_contexts.rb file
  # Note: you can only use a single hiera context per describe/context block
  # rspec-puppet does not allow you to swap out hiera data on a per test block
  # include_context :hiera

  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:facts) do
    {
      gpu_type: "nvidia",
      hostname: "000000",

    }
  end

  let(:params) do
    {
      pool_name: "nanopool",
      currency: "etn",
      owner_wallets: { etn: "sdfasdfasdfsdf" },
      miner_attrs: { name: 'xmr-stak' }

    }
  end

  on_supported_os(test_on).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it do
        is_expected.to contain_file("/home/ethos/remote.conf").with(
          ensure: "present",
          content: "# disabled",
        )
      end

      it do
        is_expected.to contain_exec("miner_service").with(
          refreshonly: true,
          command: "minestop",
        )
      end
    end
  end
end
