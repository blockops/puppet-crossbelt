require "spec_helper"
require "shared_contexts"

describe "crossbelt::rigs::ethos" do
  # by default the hiera integration uses hiera data from the shared_contexts.rb file
  # but basically to mock hiera you first need to add a key/value pair
  # to the specific context in the spec/shared_contexts.rb file
  # Note: you can only use a single hiera context per describe/context block
  # rspec-puppet does not allow you to swap out hiera data on a per test block
  # include_context :hiera

  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:facts) do
    {
      gpu_type: "amdgpu",
      hostname: "000000",
    }
  end

  let(:config_file) { "/home/ethos/local.conf" }

  let(:params) do
    {
      pool_name: "nanopool",
      currency: "etn",
      dualminer_currency: "dcr",
      dualminer_pool_name: "nanopool",
      owner_wallets: { etn: "sdfasdfasdfsdf" },
      miner_attrs: { name: 'xmr-stak' }
    }
  end

  on_supported_os(test_on).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      describe "etn" do
        let(:params) do
          {
            pool_name: "nanopool",
            currency: "etn",
            dualminer_currency: "dcr",
            dualminer_pool_name: "nanopool",
            owner_wallets: { etn: "sdfasdfasdfsdf" },
          }
        end

        describe "nanopool" do
          it { is_expected.to contain_file(config_file).with_content(%r{proxywallet sdfasdfasdfsdf}) }
          it { is_expected.to contain_file(config_file).with_content(%r{loc 1234}) }
          it { is_expected.to contain_file(config_file).with_content(%r{mem 113-58045HED1-W91 2000}) }
          it { is_expected.to contain_file(config_file).with_content(%r{## 113-58045HED1-W91 XFX 570 4GB XXX}) }
          it { is_expected.to contain_file(config_file).with_content(%r{cor 113-58045HED1-W91 1125}) }
          it { is_expected.to contain_file(config_file).with_content(%r{vlt 113-58045HED1-W91 900}) }
          it { is_expected.to contain_file(config_file).with_content(%r{pwr 113-58045HED1-W91 3}) }
          it { is_expected.to contain_file(config_file).with_content(%r{## 113-58085SHC1-W90 XFX 580 GTS}) }
          it { is_expected.to contain_file(config_file).with_content(%r{mem 113-58085SHC1-W90 2100}) }
          it { is_expected.to contain_file(config_file).with_content(%r{cor 113-58085SHC1-W90 1130}) }
          it { is_expected.to contain_file(config_file).with_content(%r{vlt 113-58085SHC1-W90 900}) }
          it { is_expected.to contain_file(config_file).with_content(%r{pwr 113-58085SHC1-W90 3}) }
        end
      end

      # uncomment the following line if you want to see the rendered output of local.comf
      # it {is_expected.to contain_file(config_file).with_content('')}

      it do
        is_expected.to contain_file("/home/ethos/remote.conf").with(
          ensure: "present",
          content: "# disabled",
        )
      end

      it do
        is_expected.to contain_exec("miner_service").with(
          refreshonly: true,
          command: "minestop",
        )
      end

      it do
        is_expected.to contain_user("ethos")
      end
    end
  end
end
