require 'spec_helper'
require 'shared_contexts'

describe 'crossbelt::setup' do
  # by default the hiera integration uses hiera data from the shared_contexts.rb file
  # but basically to mock hiera you first need to add a key/value pair
  # to the specific context in the spec/shared_contexts.rb file
  # Note: you can only use a single hiera context per describe/context block
  # rspec-puppet does not allow you to swap out hiera data on a per test block
  # include_context :hiera

  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) do
    {
      # home_dir: "/home/ethos",
      # home_user: "ethos",
      # git_url: "git@gitlab.com:coreynwops/crypto_configs.git",
      packages: ["git", "tree", 'anacron'],
      installed_packages:  ['git', 'tree']
      # repo_dir: "/home/ethos/crypto_configs",

    }
  end

  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)
  #
  on_supported_os(test_on).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      ['git', 'tree', 'anacron'].each do |package|
        it do
          is_expected.to contain_package(package).with(
            ensure: 'present',
          )
        end
      end
    end
  end
end
