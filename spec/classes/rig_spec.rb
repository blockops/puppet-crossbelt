require "spec_helper"
require "shared_contexts"

describe "crossbelt::rig" do
  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)
  let(:extra_facts) do
    {
      miner_distro: { "name" => "ethos", "version" => "1.3.3" },
      mining_profile: "ethermine",
      algo_profile: "ethash",
      remote_mining_profile: "default",
      remote_rig_owner: "opselite",
      rig_owner: "remote",
    }
  end

  on_supported_os(test_on).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      describe "ethos" do
        let(:params) do
          {
            pool_name: "suprnova.cc",
            currency: "etn",
            rig_os: "ethos",
            auto_install_miner: true,
            owner_wallets: { btc: "sdfasdfasdfsdf" },
          }
        end

        it do
          is_expected.to contain_class("crossbelt::rigs::ethos")
        end

        it "contain configuration file" do
          is_expected.to contain_file("/opt/crossbelt/config/configuration.yaml")
        end

        it "contain miner" do
          is_expected.to contain_crossbelt__install_miner("ccminer").with(
            download_url: "https://github.com/tpruvot/ccminer/archive/windows.zip",
            install_path: "/opt/crossbelt/miners/ccminer",
            checksum: nil,
            checksum_type: "sha1",
            binary_path: "/opt/crossbelt/miners/ccminer/ccminer",
          )
        end
      end

      describe "not ethos" do
        let(:params) do
          {
            pool_name: "suprnova.cc",
            currency: "etn",
            rig_os: "dafds",
            auto_install_miner: false,
            owner_wallets: { btc: "sdfasdfasdfsdf" },
          }
        end

        it do
          is_expected.to compile.and_raise_error(%r{dafds not supported})
        end
      end
    end
  end
end
