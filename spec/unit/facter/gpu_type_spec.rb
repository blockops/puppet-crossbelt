require 'spec_helper'
require 'facter'
require 'crossbelt/rig'
describe 'gpu_type', type: :fact do
  before :each do
    # perform any action that should be run before every test
    Facter.clear
    # This will mock the facts that confine uses to limit facts running under certain conditions
    allow(Facter.fact(:kernel)).to receive(:value).and_return('Linux')
    # below is how you mock responses from the command line
    # you will need to built tests that plugin different mocked values in order to fully test your facter code
    allow(Facter::Core::Execution).to receive(:execute).with('uname -r', on_fail: nil).and_return('Linux')
    allow(Facter::Core::Execution).to receive(:execute).with('uname -m', on_fail: nil).and_return('Linux')
    allow(Crossbelt::Rig.instance).to receive(:type).and_return('amdgpu')
  end

  it 'returns a value' do
    expect(Facter.fact(:gpu_type).value).to eq('amdgpu')
  end
end
