require 'spec_helper'
require 'facter'
require 'crossbelt/compute_units'
require 'facter/compute_units'

describe 'compute_units', type: :fact do
  subject(:fact) { Facter.fact(subject) }

  let(:compute_unit) do
    { 'uuid' => 'GPU-8224de15-a1f4-2f83-6fde-aaa6e2537d73',
      'gpuId' => 'GPU5',
      'syspath' => '/sys/bus/pci/devices/0000:0f:00.0',
      'pciLoc' => '0000:0f:00.0',
      'name' => 'GeForce GTX 1080',
      'bios' => '86.04.60.00.80',
      'subType' => 'nvidia',
      'make' => 'Nvidia',
      'model' => 'GeForce GTX 1080',
      'vendor' => 'Evga' }
  end

  let(:computes) do
    Array(instance_double(Crossbelt::ComputeUnit::Gpu))
  end

  before :each do
    Facter.clear
    allow(Facter.fact(:kernel)).to receive(:value).and_return('Linux')
  end

  it 'returns an empty array' do
    expect(Facter.fact(:compute_units).value).to eq([])
  end

  it 'returns items' do
    allow(Crossbelt::ComputeUnits).to receive(:find_all).and_return(computes)
    allow(computes.first).to receive(:hardware_info).and_return(compute_unit)
    expect(Facter.fact(:compute_units).value).to eq([{ 'uuid' => 'GPU-8224de15-a1f4-2f83-6fde-aaa6e2537d73',
                                                       'gpuId' => 'GPU5',
                                                       'syspath' => '/sys/bus/pci/devices/0000:0f:00.0',
                                                       'pciLoc' => '0000:0f:00.0',
                                                       'name' => 'GeForce GTX 1080',
                                                       'bios' => '86.04.60.00.80',
                                                       'subType' => 'nvidia',
                                                       'make' => 'Nvidia',
                                                       'model' => 'GeForce GTX 1080',
                                                       'vendor' => 'Evga' }])
  end
end
