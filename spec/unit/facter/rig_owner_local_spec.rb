require 'spec_helper'
require 'facter'
require 'crossbelt/rig'
describe 'rig_owner_local', type: :fact do
  before :each do
    # perform any action that should be run before every test
    Facter.clear
    # This will mock the facts that confine uses to limit facts running under certain conditions
    allow(Facter.fact(:kernel)).to receive(:value).and_return('Linux')
    allow(Crossbelt::Config).to receive(:global_config_file).and_return(File.join(fixtures_dir, 'crossbelt_config.yaml'))

  end

  it 'returns a value' do
    expect(Facter.fact(:rig_owner_local).value).to eq('opselite')
  end

  it 'returns default when fact dne' do
    allow(Crossbelt::Config).to receive(:global_config_file).and_return(File.join(fixtures_dir, 'crossbelt_config_dne.yaml'))
    expect(Facter.fact(:rig_owner_local).value).to eq('default')
  end
end
