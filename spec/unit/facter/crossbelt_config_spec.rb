require 'spec_helper'
require 'facter'
require 'crossbelt/config'
describe 'crossbelt_config', type: :fact do
  before :each do
    # perform any action that should be run before every test
    Facter.clear
    # This will mock the facts that confine uses to limit facts running under certain conditions
    allow(Facter.fact(:kernel)).to receive(:value).and_return('Linux')
    allow(Crossbelt::Config).to receive(:global_config_file).and_return(File.join(fixtures_dir, 'crossbelt_config.yaml'))
  end

  it 'returns a value' do
    expect(Facter.fact(:crossbelt_config).value).to eq(
                                                        {"rig_endpoint"=>"iot.us-east-1.amazonaws.com",
                                                         "rigId"=>1234565, "rig_name"=>"myrig1",
                                                         "data_repo_url"=>"https://gitlab.com/blockops/crossbelt_data.git",
                                                         "rig_owner"=>"opselite",
                                                         "mining_profile"=>"ethermine"})

  end

  it 'returns nothing when file does not exist' do
    allow(Crossbelt::Config).to receive(:global_config_file).and_return(File.join(fixtures_dir, 'crossbelt_config_dne.yaml'))
    expect(Facter.fact(:crossbelt_config).value).to eq(nil)
  end
end
