require 'spec_helper'
require 'facter'
require 'facter/site_id'
require 'crossbelt/rig'
describe 'site_id', type: :fact do
  after(:each) { Facter.clear }

  before :each do
    Facter.clear
    # perform any action that should be run before every test
    # This will mock the facts that confine uses to limit facts running under certain conditions
    allow(Facter.fact(:kernel)).to receive(:value).and_return('Linux')
  end

  let(:rig) do
    double
  end

  let(:fact) do
    Facter.fact(subject)
  end

  it 'returns a value' do
    allow(rig).to receive(:location).and_return('1234567890')
    allow(Crossbelt::Rig).to receive(:instance).and_return(rig)
    expect(fact.value).to eq('1234567890')
  end
end
