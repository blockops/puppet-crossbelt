require 'spec_helper'
require 'facter'
require 'facter/miner_distro'

describe 'miner_distro', type: :fact do
  let(:fact) do
    Facter.fact(:miner_distro)
  end

  before :each do
    Facter.clear
    # perform any action that should be run before every test
    allow(Facter.fact(:kernelrelease)).to receive(:value).and_return('4.15.12-ethos83')
    # This will mock the facts that confine uses to limit facts running under certain conditions
    allow(Facter.fact(:kernel)).to receive(:value).and_return('Linux')
  end

  it 'returns a value' do
    allow(File).to receive(:exist?).with('/opt/ethos/etc/version').and_return(true)
    allow(File).to receive(:read).with('/opt/ethos/etc/version').and_return('1.3.1')
    expect(fact.value).to eq('name' => 'ethos', 'version' => '1.3.1')
  end
end
