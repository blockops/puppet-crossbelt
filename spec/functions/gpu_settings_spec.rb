require 'spec_helper'

describe 'crossbelt::gpu_settings' do
  # before do
  #   # allow(scope).to receive(:lookup).with('crossbelt::config::cor', anything).and_return('1150')
  #   # allow(scope).to receive(:lookup).with('crossbelt::config::mem', anything).and_return('2000')
  #   # allow(scope).to receive(:lookup).with('crossbelt::config::vlt', anything).and_return('900')
  #   # allow(scope).to receive(:lookup).with('crossbelt::config::pwr', anything).and_return('100')
  # end

  on_supported_os(test_on).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge('gpu_type' => 'amdgpu') }

      describe 'when defined' do
        let(:arg1) do
          '113-58045HED1-W91'
        end

        it do
          is_expected.to run.with_params(arg1).and_return(
            '# 113-58045HED1-W91' => 'XFX 580 4GB XXX',
            'cor 113-58045HED1-W91' => '1100',
            'vlt 113-58045HED1-W91' => '900',
            'mem 113-58045HED1-W91' => '2100',
            '# pwr 113-58045HED1-W91' => '',
            'dpm 113-58045HED1-W91' => '3',
          )
        end
      end
      describe 'when not defined' do
        let(:facts) do
          {
            gpu_type: 'amdgpu',
          }
        end
        let(:arg1) do
          '113-58045HED1-W22'
        end

        it do
          is_expected.to run.with_params(arg1).and_return(
            '# 113-58045HED1-W22' => '#',
            'cor 113-58045HED1-W22' => '1150',
            '# vlt 113-58045HED1-W22' => '',
            'mem 113-58045HED1-W22' => '1950',
            '# pwr 113-58045HED1-W22' => '',
            'dpm 113-58045HED1-W22' => '3',
          )
        end
      end
    end
  end
end
