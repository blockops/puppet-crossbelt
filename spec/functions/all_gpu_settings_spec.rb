require 'spec_helper'
describe 'crossbelt::all_gpu_settings' do
  before(:each) do
    allow(Facter::Core::Execution).to receive(:execute).with('uname -r', on_fail: nil).and_return('Linux')
    allow(Facter::Core::Execution).to receive(:execute).with('hostname').and_return('000000')
  end
  let(:facts) do
    {
      gpu_type: 'nvidia',
      hostname: '000000',
    }
  end

  let(:compute_units) do
    JSON.parse(File.read(File.join(samples_dir, 'compute_units.json')))
  end

  let(:expected) do
    [
      { '# 86.04.50.00.46' => '#',
        '# cor 86.04.50.00.46' => '',
        '# vlt 86.04.50.00.46' => '',
        '# mem 86.04.50.00.46' => '',
        '# pwr 86.04.50.00.46' => '',
        'dpm 86.04.50.00.46' => '5' },
      { '# 86.04.50.00.64' => '#',
        '# cor 86.04.50.00.64' => '',
        '# vlt 86.04.50.00.64' => '',
        '# mem 86.04.50.00.64' => '',
        '# pwr 86.04.50.00.64' => '',
        'dpm 86.04.50.00.64' => '5' },
      { '# 86.04.50.00.72' => '#',
        '# cor 86.04.50.00.72' => '',
        '# vlt 86.04.50.00.72' => '',
        '# mem 86.04.50.00.72' => '',
        '# pwr 86.04.50.00.72' => '',
        'dpm 86.04.50.00.72' => '5' },
      { '# 86.04.1E.00.23' => '#',
        '# cor 86.04.1E.00.23' => '',
        '# vlt 86.04.1E.00.23' => '',
        '# mem 86.04.1E.00.23' => '',
        '# pwr 86.04.1E.00.23' => '',
        'dpm 86.04.1E.00.23' => '5' },
    ]
  end

  on_supported_os(test_on).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it do
        is_expected.to run.with_params(compute_units).and_return(expected)
      end
    end
  end
end
