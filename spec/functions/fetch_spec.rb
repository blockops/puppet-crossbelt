require 'spec_helper'

describe 'crossbelt::fetch' do
  on_supported_os(test_on).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      let(:args) do
        [
          { 'key' => 'value' }, 'key'
        ]
      end

      it { is_expected.to run.with_params(args[0], args[1]).and_return('value') }
    end
  end
end
