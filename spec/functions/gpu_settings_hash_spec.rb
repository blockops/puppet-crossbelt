require "spec_helper"

describe "crossbelt::gpu_settings_hash" do
  let(:facts) do
    { mining_profile: "ethermine", rig_owner: "opselite" }
  end

  let(:gpu_bios_name) do
    "86.04.3B.40.64"
  end
  it {
    is_expected.to run.with_params(gpu_bios_name)
                     .and_return({ "86.04.3B.40.64" => { "desc" => "# Gigabyte 1080",
                                                         "cor" => "",
                                                         "vlt" => "",
                                                         "mem" => "+900",
                                                         "pwr" => "180",
                                                         "dpm" => "5",
                                                         "oc_profile" => nil } })
  }
end
