require 'spec_helper'

describe 'crossbelt::gpu_rig_settings' do
  on_supported_os(test_on).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it do
        is_expected.to run.with_params.and_return(
          '# Rig Specific Settings for foo' => '#',
          'loc foo' => '', 'cor foo' => '',
          'vlt foo' => '', 'mem foo' => '',
          'pwr foo' => '', 'dpm foo' => '5',
          'pduname foo' => '', 'pduip foo' => '',
          'pduport foo' => '', 'safevolt' => 'disabled'
        )
      end
    end
  end
end
