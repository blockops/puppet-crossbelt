require 'spec_helper'

describe 'crossbelt::gpu_settings_hash' do
  let(:gpu_bios_name) do
    'some_value_goes_here'
  end
  it { is_expected.to run.with_params(gpu_bios_name).and_return('some_value') }
end
