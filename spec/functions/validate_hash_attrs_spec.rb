require 'spec_helper'

describe 'crossbelt::validate_hash_attrs' do
  
  it { is_expected.to run.with_params( { 'name' => 'xmr-stak' }).and_return(true) }
  it { is_expected.to run.with_params( { 'name' => 'xmr-stak', 'binary_path' => nil }).and_return(false) }
  it { is_expected.to run.with_params( { 'name' => 'xmr-stak', 'binary_path' => nil }, {'binary_path' => true}).and_return(true) }
end
