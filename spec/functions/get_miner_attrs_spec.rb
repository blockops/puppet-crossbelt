require "spec_helper"

describe "crossbelt::get_miner_attrs" do
  let(:miner) do
    "cast_xmr-vega"
  end
  it {
    is_expected.to run.with_params(miner)
                     .and_return({ "checksum" => "9241607ce90a8369e9c90dc0393067344bd4c908",
                                   "checksum_type" => "sha1",
                                   "download_url" => "http://www.gandalph3000.com/download/cast_xmr-vega-ubuntu_171.tar.gz",
                                   "install_path" => "/opt/crossbelt/miners/cast_xmr-vega-ubuntu_171",
                                   "binary_path" => "/opt/crossbelt/miners/cast_xmr-vega-ubuntu_171/cast_xmr-vega" })
  }
end
