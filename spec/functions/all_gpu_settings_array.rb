require 'spec_helper'

describe 'crossbelt::all_gpu_settings_array' do
  let(:compute_units) do
    'some_value_goes_here'
  end
  it { is_expected.to run.with_params(compute_units).and_return('some_value') }
end
