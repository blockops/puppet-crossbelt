require 'spec_helper'

describe 'crossbelt::getminer' do
  on_supported_os(test_on).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts }

      it do
        # allow(scope).to receive(:lookupvar).with('gpu_type').and_return('amdgpu')
        is_expected.to run.with_params('etn', 'amdgpu').and_return('claymore-xmr')
      end
    end
  end
end
