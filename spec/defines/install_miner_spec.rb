require "spec_helper"
require "shared_contexts"

describe "crossbelt::install_miner" do
  # by default the hiera integration uses hiera data from the shared_contexts.rb file
  # but basically to mock hiera you first need to add a key/value pair
  # to the specific context in the spec/shared_contexts.rb file
  # Note: you can only use a single hiera context per describe/context block
  # rspec-puppet does not allow you to swap out hiera data on a per test block
  #include_context :hiera

  let(:title) { "cast_xmr-vega" }

  # below is the facts hash that gives you the ability to mock
  # facts on a per describe/context block.  If you use a fact in your
  # manifest you should mock the facts below.
  let(:extra_facts) do
    {}
  end

  # below is a list of the resource parameters that you can override.
  # By default all non-required parameters are commented out,
  # while all required parameters will require you to add a value
  let(:params) do
    {
      download_url: "http://www.gandalph3000.com/download/cast_xmr-vega-ubuntu_171.tar.gz",
      checksum: "9241607ce90a8369e9c90dc0393067344bd4c908",
      checksum_type: "sha1",
      binary_path: "/opt/crossbelt/miners/cast_xmr-vega-ubuntu_171/cast_xmr-vega",
      required_packages: ["xy"],
    }
  end
  # add these two lines in a single test block to enable puppet and hiera debug mode
  # Puppet::Util::Log.level = :debug
  # Puppet::Util::Log.newdestination(:console)
  on_supported_os(test_on).each do |os, os_facts|
    context "on #{os}" do
      let(:facts) { os_facts.merge(extra_facts) }

      it { is_expected.to compile }

      it do
        is_expected.to contain_package("xy").with(
          ensure: "present",
        )
      end

      it do
        is_expected.to contain_archive("/tmp/cast_xmr-vega-ubuntu_171.tar.gz").with(
                         ensure: "present",
                         extract: true,
                         extract_path: "/opt/crossbelt/miners",
                         source: "http://www.gandalph3000.com/download/cast_xmr-vega-ubuntu_171.tar.gz",
                         checksum: "9241607ce90a8369e9c90dc0393067344bd4c908",
                         checksum_type: "sha1",
                         creates: "/opt/crossbelt/miners/cast_xmr-vega-ubuntu_171/cast_xmr-vega",
                         cleanup: true,
                       )
      end

      it do
        is_expected.to contain_file("/usr/bin/cast_xmr-vega").with(
                         ensure: "link",
                         target: "/opt/crossbelt/miners/cast_xmr-vega-ubuntu_171/cast_xmr-vega",
                       )
                         .that_requires("Archive[/tmp/cast_xmr-vega-ubuntu_171.tar.gz]")
      end
    end
  end
end
