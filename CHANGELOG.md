# Changelog

All notable changes to this project will be documented in this file.

## Release 0.6.0

### Features
 * Adds ability to control ssh authorized keys
