#!/usr/bin/env bash
# https://teppen.io/2018/06/23/aws_s3_etags/
channel='unstable'
cb_file="crossbelt-latest-${channel}.deb"
cb_url="https://crossbelt.blockops.party/downloads/${cb_file}"
REMOTE_CB_FILE_SUM=$(curl --head ${CB_URL} -s |grep ETag | cut -d \: -f2)
echo "crossbelt_latest_md5=${REMOTE_CB_FILE_SUM}"
