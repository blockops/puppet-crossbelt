plan crossbelt::compute_unit::status(
  TargetSpec $nodes = [],
  Enum[json] $format = 'json'
) {
  $task_results = run_task('crossbelt::status', $nodes, output_type => 'json', _catch_errors => true)

  # protect agains failed nodes
  # protect against illegal output
  $data = $task_results.reduce([]) | $agg, $result | {
    $result['status'] == 'success' ?  { true => $agg + parsejson($result.value[_output]), default => $agg }
  }
  return $data
}