plan crossbelt::rig_status(
  TargetSpec $nodes = [],
) {
  $exporters = 'json,stdout'
  $task_results = run_task('crossbelt::status', $nodes, exporters => $exporters)
  # protect agains failed nodes
  # protect against illegal output
  $output = $task_results.reduce([]) | $agg, $result | {
    $agg + parsejson($result.value[_output]).map |Hash $cu | {
      $cu.merge({rig_name => $result.target.name})
    }
  }
  return $output
}

# better do this in ruby code instead
# https://github.com/puppetlabs/bolt/blob/master/spec/integration/remote_spec.rb#L52