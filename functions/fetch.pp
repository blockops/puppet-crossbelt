# See https://docs.puppet.com/puppet/4.5/reference/lang_write_functions_in_puppet.html
# for more information on native puppet functions.

# @example finding local_mining_profile in the facts hash
#
#   crossbelt::fetch($::facts, 'local_mining_profile', {lookup => 'crossbelt::mining_profile', 'default_value' => 'default'})
#
function crossbelt::fetch(Hash $hash, String $key, Hash $options = {}) {
  $value = $hash[$key]
  if $value {
    $value
  } elsif $options['lookup'] {
    lookup($options['lookup'])
  }
  else {
    undef
  }
}
