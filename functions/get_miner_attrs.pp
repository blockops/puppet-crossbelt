function crossbelt::get_miner_attrs(String $miner) {
  {
    checksum      => lookup('crossbelt::miner::checksum', {default_value => undef}),
    required_packages => lookup('crossbelt::miner::required_packages', {default_value => []}),
    checksum_type => lookup('crossbelt::miner::checksum_type', {default_value => undef}),
    download_url  => lookup('crossbelt::miner::download_url', {default_value => undef}),
    install_path  => lookup('crossbelt::miner::install_path', {default_value => undef}),
    binary_path   => lookup('crossbelt::miner::binary_path', {default_value => undef}),
    name          => lookup('crossbelt::miner::base_name', {default_value => $miner})
  }
}
