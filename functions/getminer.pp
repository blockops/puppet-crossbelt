# See https://docs.puppet.com/puppet/4.5/reference/lang_write_functions_in_puppet.html
# for more information on native puppet functions.

# Example Documention for function using puppet strings
# https://github.com/puppetlabs/puppetlabs-strings

function crossbelt::getminer(String $currency, String $gpu_type) {
  $algo = lookup('crossbelt::algo')
  $miners = lookup('crossbelt::config::globalminer')
  # $miners should be a hash but if it is a string, use that
  $gpu_miner = $miners =~ String ? { true => $miners, false => $miners[$gpu_type] }
  unless $gpu_miner { fail("Cannot find miner program for ${facts[gpu_type]} and ${currency}") }
  $gpu_miner
}
