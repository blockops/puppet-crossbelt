# See https://docs.puppet.com/puppet/4.5/reference/lang_write_functions_in_puppet.html
# for more information on native puppet functions.

# given a bios name ie. 113-1E3660U-051 lookup all the configs in hiera
# @return [Hash]
# Example Return value
#      { "cor 113-58045HED1-W91 => 1150",
#       "vlt 113-58045HED1-W91 => 900",
#       "mem 113-58045HED1-W91 => 2000",
#       "pwr 113-58045HED1-W91 => 3" }
function crossbelt::gpu_settings(String $gpu_bios_name) {
  # if a gpu configs are defined we reference the gpu config and use the values from those
  $gpu_configs = lookup('crossbelt::config::gpu_configs', {default_value => {}})
  if $gpu_configs {
    $gpu_config = $gpu_configs[$gpu_bios_name.upcase]
  }
  $cor = lookup('crossbelt::config::cor', {default_value => undef})
  $vlt = lookup('crossbelt::config::vlt', {default_value => undef})
  $mem = lookup('crossbelt::config::mem', {default_value => undef})
  $pwr = lookup('crossbelt::config::pwr', {default_value => undef})
  $desc = lookup('crossbelt::config::desc', {default_value => '#'})
  $dpm = lookup('crossbelt::config::dpm', {default_value => '5'})
  $values = {
    "# ${gpu_bios_name}" => $desc,
    "cor ${gpu_bios_name}" => String($cor),
    "vlt ${gpu_bios_name}" => String($vlt),
    "mem ${gpu_bios_name}" => String($mem),
    "pwr ${gpu_bios_name}" => String($pwr),
    "dpm ${gpu_bios_name}" => String($dpm)

  }
  $values_z = $values.map |$key, $value| {
    if empty($value) {
      { "# ${key}" => $value }
    } else {
      { $key => $value }
    }
  }
  merge(*$values_z)
}
