# See https://docs.puppet.com/puppet/4.5/reference/lang_write_functions_in_puppet.html
# for more information on native puppet functions.

# @return [Hash]
function crossbelt::gpu_rig_settings() {
  $cor = lookup('crossbelt::config::rig::cor', {default_value => undef})
  $vlt = lookup('crossbelt::config::rig::vlt', {default_value => undef})
  $mem = lookup('crossbelt::config::rig::mem', {default_value => undef})
  $pwr = lookup('crossbelt::config::rig::pwr', {default_value => undef})
  $desc = lookup('crossbelt::config::rig::desc', {default_value => '#'})
  $loc  = $::facts.get('crossbelt_config.rig_name', lookup('crossbelt::config::rig::loc', {default_value => undef}))
  $pduname  = $::facts.get('crossbelt_config.pduname', lookup('crossbelt::config::rig::pduname', {default_value => undef}))
  $pduip  = $::facts.get('crossbelt_config.pduip', lookup('crossbelt::config::rig::pduip', {default_value => undef}))
  $pduport  = $::facts.get('crossbelt_config.pduport', lookup('crossbelt::config::rig::pduport', {default_value => undef}))

  $safevault = lookup('crossbelt::config::rig::safevolt', {default_value => 'disabled'})
  $dpm = lookup('crossbelt::config::rig::dpm', {default_value => '5'})

  $values = {
    "# Rig Specific Settings for ${::hostname}" => $desc,
    "loc ${::hostname}" => String($loc),
    "cor ${::hostname}" => String($cor),
    "vlt ${::hostname}" => String($vlt),
    "mem ${::hostname}" => String($mem),
    "pwr ${::hostname}" => String($pwr),
    "dpm ${::hostname}" => String($dpm),
    "pduname ${::hostname}" => String($pduname),
    "pduip ${::hostname}" => String($pduip),
    "pduport ${::hostname}" => String($pduport),
    'safevolt'          => String($safevault)
  }
}
