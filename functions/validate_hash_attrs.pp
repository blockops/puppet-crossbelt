# return true if no undef values, false otherwise
function crossbelt::validate_hash_attrs(Hash $attrs, Hash $empty_values = {}) {
  $total = $attrs.reduce(0) | Integer $acc, $hash | {
    $key = $hash[0]
    $value = $hash[1]
    $score = (($value !~ Undef) or $empty_values.has_key($key)) ? { true => 0 , false => 1  }
    $score + $acc
  }
  $total < 1
}
