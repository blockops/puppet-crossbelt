# See https://docs.puppet.com/puppet/4.5/reference/lang_write_functions_in_puppet.html
# for more information on native puppet functions.

# returns something like
#  => [
#   [0] {
#     "113-58085SHC1-W90" => [
#       [0] "## 113-58085SHC1-W90 ",
#       [1] "cor 113-58085SHC1-W90 1150",
#       [2] "vlt 113-58085SHC1-W90 900",
#       [3] "mem 113-58085SHC1-W90 2000",
#       [4] "pwr 113-58085SHC1-W90 3"
#     ]
#   },
#   [1] {
#     "113-58085SHC1-W90" => [
#       [0] "## 113-58085SHC1-W90 ",
#       [1] "cor 113-58085SHC1-W90 1150",
#       [2] "vlt 113-58085SHC1-W90 900",
#       [3] "mem 113-58085SHC1-W90 2000",
#       [4] "pwr 113-58085SHC1-W90 3"
#     ]
#   },
#   [2] {
#     "113-58045HED1-W91" => [
#       [0] "## 113-58045HED1-W91 ",
#       [1] "cor 113-58045HED1-W91 1150",
#       [2] "vlt 113-58045HED1-W91 900",
#       [3] "mem 113-58045HED1-W91 2000",
#       [4] "pwr 113-58045HED1-W91 3"
#     ]
#   }
# ]

# @param data [Hash] - output of atiflash -ai or fact containing same info
# @return [Hash] - bios name is the key, value is the values for overclocking
function crossbelt::all_gpu_settings_hash($compute_units) {
  unless $compute_units {
    fail('No data specified to all_gpu_settings()')
  }
  $output = $compute_units.map | $cu | {
    $gpu_settings = crossbelt::gpu_settings_hash($cu['bios'].upcase)
  }.unique
  # convert into hash
  if $output.count > 1 {
    merge(*$output)
  } else {
    $output
  }

}
