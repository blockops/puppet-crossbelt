class crossbelt::config(
  String $currency,
  String $pool_name,
  Hash[String, String] $owner_wallets,
) {
  $proxypool1 = lookup('crossbelt::config::proxypool1',  {default_value => ''})
  $proxypool2 = lookup('crossbelt::config::proxypool2', {default_value => ''})
  $miner = crossbelt::getminer($currency, $::gpu_type) # required for hiera
  $miner_flags_settings = lookup('crossbelt::config::miner::settings')
  $miner_conf = crossbelt::get_miner_attrs($miner)
  $miner_flags = $miner_flags_settings.filter |$f| { ! empty($f) }
  $gpu_rig_settings = crossbelt::gpu_rig_settings()
  $pduname  = lookup('crossbelt::config::rig::pduname', {default_value => undef})
  $pduip  = lookup('crossbelt::config::rig::pduip', {default_value => undef})
  $pduport  = lookup('crossbelt::config::rig::pduport', {default_value => undef})
  $remote_miner_data = lookup('crossbelt::config::miner_data', {default_value => {}})
  $miner_url = ''
  $miner_data = merge({name => $miner, conf => $miner_conf},$remote_miner_data, $miner_flags )
  $configuration_content = {
    rig_settings => $gpu_rig_settings,
    gpu_settings => crossbelt::all_gpu_settings_hash($facts['compute_units']),
    wallets      => $owner_wallets,
    miner_data   => $miner_data,
    mining_profile => $::mining_profile,
    algo_profile   => $::algo_profile,
    rig_owner      => $::rig_owner,
    remote_rig_owner => $::remote_rig_owner,
    remote_mining_profile => $::remote_mining_profile,
    pool_name      => $pool_name,
    pool_user      => $owner_wallets[$currency],
    pool_pass      => 'x',
    pools          => [$proxypool1, $proxypool2]
  }

  #disabling automatic reconfigure
  file{'/opt/crossbelt/config/configuration.yaml':
    ensure  => present,
    content => template('crossbelt/configuration.yaml.erb'),
  }
}
