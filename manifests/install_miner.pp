define crossbelt::install_miner(
  Stdlib::HTTPUrl $download_url,
  Optional[String] $checksum,
  Optional[String] $checksum_type,
  Stdlib::Absolutepath $binary_path,
  Array[String] $required_packages = [],
  Stdlib::Absolutepath $install_path = '/opt/crossbelt/miners',
  Stdlib::Absolutepath $alias_path = "/usr/bin/${basename($binary_path)}"
) {

  # replace with extlib functions
  $dirs = $install_path.split('/').reduce([]) |Array $acc, $value  | {
    $counter = $acc.length - 1
    unless empty($value) {
      $acc + "${$acc[$counter]}/${value}"
    } else {
      $acc
    }
  }

  @file{$dirs:
    ensure => directory
  }
  realize(File[$dirs])

  ensure_packages($required_packages, { ensure => present})

  archive{"/tmp/${basename($download_url)}":
    ensure        => present,
    extract       => true,
    extract_path  => $install_path,
    source        => $download_url,
    checksum      => $checksum,
    checksum_type => $checksum_type,
    creates       => $binary_path,
    cleanup       => true,
    require       => [File[$dirs], Package[$required_packages]]
  }
  -> file{$alias_path:
    ensure => link,
    target => $binary_path
  }
  -> file{$binary_path:
    ensure => file,
    mode   => '0555'
  }
}

