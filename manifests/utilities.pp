class crossbelt::utilities(
  $home_user = 'ethos',
  $dev_packages = ['autoconf', 'libcurl4-nss-dev', 'libcurl4-openssl-dev', 'pkg-config', 'libtool', 'libncurses5-dev']
) {
  # cpan && Data::Dumper
  ensure_packages($dev_packages, {ensure => present})

}
