class crossbelt::setup (
  Array[String] $users = [],
  String $home_dir = '/root',
  String $home_user = 'root',
  Array[String] $installed_packages = [],
  Array[String] $removed_packages = [],
  Array[String] $packages = ['git', 'tree', 'anacron', 'anacron'],
) {
  contain crossbelt::ssh
  $ssh_keys = lookup('crossbelt::ssh::authorized_keys', {default_value => {}})
  $ssh_users = $ssh_keys.map |String $alias, Hash $metadata| {
    $metadata.dig('user')
  }
  ($ssh_users + $users).unique.each |$user| {
    user{$user:
        ensure         => present,
        purge_ssh_keys => false,
        managehome     => true,
        before         => Class['crossbelt::ssh']
    }
  }

  Exec{
    environment => ["HOME=${home_dir}", "USER=${home_user}"]
  }
  ensure_packages($packages, {ensure => present})
  ensure_packages($installed_packages, {ensure => present})
  ensure_packages($removed_packages, {ensure => absent})
  service{'anacron': ensure => running, enable => true, require => Package['anacron'] }

  class{'logrotate':
    ensure => present,
  }

  logrotate::rule { 'crossbelt':
    path         => '/var/log/crossbelt/*.log',
    rotate       => 3,
    rotate_every => 'month',
  }

  ini_setting { 'reports':
    ensure  => absent,
    section => 'user',
    setting => 'reports',
    path    => "${settings::confdir}/puppet.conf",
  }

  #class { '::lm_sensors': }
  # class { 'nodejs':
  #   repo_url_suffix => '8.x',
  # } ->
  # package { 'pm2':
  #   ensure   => 'present',
  #   provider => 'npm',
  # }

}
