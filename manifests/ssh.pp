class crossbelt::ssh(
  Crossbelt::Authorizedkeys $authorized_keys = {},
) {
  $authorized_keys.each |String $key_name, Crossbelt::Authorizedkey $metadata| {
    $key_title = $metadata.get('alias', $key_name)
    $user      = $metadata.get('user')
    $key_type = $metadata.get('ssh-rsa', 'ssh-rsa')
    $key_ensure = $metadata.get('ensure', 'present')

    ssh_authorized_key { $key_title:
      ensure => $key_ensure,
      user   => $user,
      type   => $key_type,
      key    => $metadata['key']
    }
  }
}
