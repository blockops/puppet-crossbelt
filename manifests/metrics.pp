class crossbelt::metrics(
  Float $power_cost = 0.09,
  Float $total_mining_fees = 0.04,
  Float $hardware_cost = 0.0
) {
  $metric_data = {
    power_cost => $power_cost,
    total_mining_fees => $total_mining_fees,
    hardware_cost => $hardware_cost,
  }
  file{'/opt/crossbelt/rig_cost_data.json':
    ensure  => present,
    content => $metric_data.to_json
  }
  # class { '::telegraf':
  #   hostname => $::hostname,
  #   manage_repo => false,
  #   outputs  => {
  #     'influxdb' => {
  #       'urls'     => [ 'http://159.89.32.246:8086'],
  #       'database' => 'telegraf',
  #       'username' => 'nwops',
  #       'password' => 'nwops1',
  #     }
  #   },
  #   inputs   => {
  #     mem => {},
  #     sensors => {},
  #     io => {},
  #     disk => {},
  #     net => {},
  #     system => {},
  #     'cpu' => {
  #       'percpu'   => true,
  #       'totalcpu' => true,
  #     },
  #   }
  # }

  # telegraf::input { 'total_hash_rate':
  #   plugin_type => 'exec',
  #   options     => {
  #     'commands'    => ["/opt/puppetlabs/puppet/bin/ruby /opt/crossbelt/metrics/ethos_total_hash.rb"],
  #     'name_suffix' => 'total_hash_rate',
  #     'interval' => '30s',
  #     'data_format' => 'value',
  #     'data_type' => 'integer'
  #   }
  # }
  #
  # telegraf::input { 'GPU_Power':
  #   plugin_type => 'exec',
  #   options     => {
  #     'commands'    => ["/opt/puppetlabs/puppet/bin/ruby /opt/crossbelt/metrics/gpu_power_metrics_amd.rb"],
  #     'name_suffix' => 'GPU_Power',
  #     'interval' => '30s',
  #     'data_format' => 'json',
  #     'data_type' => 'integer'
  #   }
  # }
}