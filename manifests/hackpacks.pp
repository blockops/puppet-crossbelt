class crossbelt::hackpacks(
  Array[String] $installed = [], # class names
  Array[String] $removed = [], # class names
) {

  $installed.each |String $klass| {
    class{"crossbelt::hackpacks::${klass}": ensure => present }
  }
  $removed.each |String $klass| {
    class{"crossbelt::hackpacks::${klass}": ensure => absent }
  }
}
