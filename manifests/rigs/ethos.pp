class crossbelt::rigs::ethos(
  Hash $miner_attrs,
  String $pool_name,
  String $currency,
  Hash[String, String] $owner_wallets,
  String $user = 'ethos',
  Stdlib::Absolutepath $local_config_path = '/home/ethos/local.conf',
  Stdlib::Absolutepath $remote_config_path = '/home/ethos/remote.conf',
  Optional[Variant[Stdlib::HTTPUrl, Enum['# disabled']]] $remote_config_url = '# disabled',
  Boolean $dualminer_enabled = false,
  Optional[String] $dualminer_currency = 'dcr',
  Optional[String] $dualminer_pool_name = undef,
  Boolean $auto_install_miner = str2bool($::facts.get('crossbelt_config.auto_install_miner', false))
  ) {
    if $::facts['miner_distro']['name'] == 'ethos' and SemVer($::facts['miner_distro']['version']) < SemVer('1.3.1') {
      fail('Ethos prior to version 1.3.1 is not supported please upgrade')
    }

    if $dualminer_enabled { $dual_miner = 'enabled' }
    else { $dual_miner = 'disabled' }

    $config_settings = lookup('crossbelt::rigs::ethos::config_settings')
    #crossbelt support multiple wallets, define a wallets key in your data with the address for each coin you want to support

    $wallets = {
      'proxywallet' => $owner_wallets.get($currency),
      'dualminer-wallet' => $owner_wallets.get($dualminer_currency)
    }

    # sets the per gpu OC settings that are located on the system
    $all_gpu_settings = crossbelt::all_gpu_settings($facts['compute_units'])
    $gpu_rig_settings = crossbelt::gpu_rig_settings()
    if $dualminer_enabled {
      # you will also need to merge these into all_global_settings, currently this is disabled
      # since dual mining is no longer possible
      $dual_miner_configs = merge(lookup('crossbelt::rigs::ethos::dualminer_config_settings'), {'dualminer' => $dual_miner } )
    }
    # If this does not resolve, the whole thing falls apart
    $miner = $miner_attrs.get('name', crossbelt::getminer($currency, $::gpu_type))
    $global_miner = { globalminer => $miner }
    $miner_flags_settings = lookup('crossbelt::config::miner::settings')
    $miner_flags = $miner_flags_settings.filter |$f| { ! empty($f) }.map |$key, $value| { "${miner}=${key} ${value}" }

    # we may need to sanitize all the key values here
    $all_global_settings = merge($config_settings, $global_miner, $wallets)

    # disabling automatic reconfigure
    file{$local_config_path:
      ensure  => present,
      content => template('crossbelt/ethos.conf.erb'),
      #notify  => Exec['miner_service'] # ethos does this for us, or we may not want to always restart the service
    }
    file{$remote_config_path:
      ensure  => present,
      content => $remote_config_url,
      notify  => Exec['miner_service']
    }
    exec{'miner_service':
      path        => ['/opt/ethos/bin', '/usr/bin', '/usr/local/bin'],
      refreshonly => true,
      command     => 'minestop',
    }

    # syncs the contents of the download miner to the ethos directories
    if $auto_install_miner {
      $miner_dir_name = $miner_attrs.get('binary_path', '.').dirname
      if $miner_dir_name =~ Stdlib::Absolutepath {
        file {"/opt/miners/${miner}":
                ensure  => directory,
                source  => $miner_dir_name,
                recurse => true,
                force   => true,
              }
      }
    }
  }
