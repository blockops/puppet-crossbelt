# A description of what this class does
#
# @summary A short summary of the purpose of this class
#
# @example
#   include crossbelt::repository
class crossbelt::repository(
  Enum['unstable', 'testing', 'stable'] $channel = 'stable'
) {
  if $facts[os][family] == 'debian' {
    include apt

    apt::source { 'crossbelt':
      comment  => 'Crossbelt repo for unstable releases',
      location => 'https://s3.amazonaws.com/omnibus-crossbelt',
      release  => $channel,
      repos    => 'main',
      pin      => '-10',
      key      => {
        'id'     => '2A7BD0A72F5625E662E6B90596B8D510AD1AD628',
        'server' => 'subkeys.pgp.net',
      },
      include  => {
        'src' => true,
        'deb' => true,
      },
    }
  } else {
    warning("${$facts[os][family]} is not supported at this time")
  }




}
