class crossbelt::install(
  String $ensure = 'present',
  Enum[stable,unstable,testing] $channel = 'unstable'
) {
  $cb_file="crossbelt-latest-${channel}.deb"
  $file_location = "/tmp/${cb_file}"
  $cb_url="https://crossbelt.blockops.party/downloads/${cb_file}"

  archive { $file_location:
    ensure        => present,
    source        =>  $cb_url,
    checksum      => $crossbelt_latest_md5,
    checksum_type => 'md5',
    cleanup       => false,
  }

  package { 'crossbelt':
    ensure   => 'present',
    provider => 'apt',
    source   => $file_location
  }
}
