class crossbelt::rig(
  String $pool_name,
  String $currency,
  Hash[String, String] $owner_wallets,
  Optional[String] $dualminer_currency,
  Optional[String] $dualminer_pool_name,
  Boolean $dualminer_enabled = false,
  String $rig_os = 'ethos',
  Boolean $auto_install_miner = $::facts.get('crossbelt_config.auto_install_miner', false),
) {
  $currency_data = {
    currency => $currency,
    dual_currency_enabled => $dualminer_enabled,
    dual_currency => $dualminer_currency,
    algorithm => lookup('crossbelt::algo')
    #dual_algorithm => lookup('crossbelt::algo')
  }
  $miner = crossbelt::getminer($currency, $::gpu_type) # required for hiera
  $miner_attrs = crossbelt::get_miner_attrs($miner)
  if $auto_install_miner {
    info("miner ${miner} will be installed")
    # looks up all the data via a function before setting the parameters for the miner resource
    $valid_miner_attrs = crossbelt::validate_hash_attrs($miner_attrs, {'checksum' => false,
      'checksum_type' => false, 'required_packages' => false})
    if $valid_miner_attrs {
      crossbelt::install_miner { $miner:
            *     => $miner_attrs
      }
    } else {
      warning("Invalid miner data specified for ${miner}, not auto installing")
    }
  }

  file{'/opt/crossbelt/currency_data.json':
    ensure  => present,
    content => $currency_data.to_json
  }
  class{'crossbelt::config':
    currency      => $currency,
    pool_name     => $pool_name,
    owner_wallets => $owner_wallets
  }

  case $rig_os {
    'ethos': {
      class{'crossbelt::rigs::ethos':
        pool_name           => $pool_name,
        currency            => $currency,
        dualminer_enabled   => $dualminer_enabled,
        dualminer_currency  => $dualminer_currency,
        dualminer_pool_name => $dualminer_pool_name,
        owner_wallets       => $owner_wallets,
        miner_attrs         => $miner_attrs,
        auto_install_miner  => $auto_install_miner,
      }
    }
    default: {
      fail("${rig_os} not supported")
    }
  }

}
