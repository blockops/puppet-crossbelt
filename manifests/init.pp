class crossbelt(
  Hash[String, String] $wallets,
){

  include crossbelt::setup
  class{'crossbelt::rig':
    owner_wallets => $wallets,
  }
  include crossbelt::hackpacks
  include crossbelt::utilities
}
