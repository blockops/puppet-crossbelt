class crossbelt::hackpacks::facette(
  Enum[present, absent] $ensure = present,
) {
  class { 'facette':
  providers => { 'collectd' => {
    'connector' => {
        'path'    => '/var/lib/collectd/rrd',
        'pattern' => '(?P<source>[^/]+)/(?P<metric>.+).rrd',
        'type'    => 'rrd',
  }}},
  }


}


