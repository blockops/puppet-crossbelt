class crossbelt::hackpacks::set_timezone(
  Enum[present, absent] $ensure = present,
) {

  class { 'timezone':
    timezone => $facts['location']['timezone'],
  }

}
