class crossbelt::hackpacks::oh_my_zsh(
    Enum[present, absent] $ensure = present,
    Array[String] $plugins = ['git', 'ssh-agent']
) {
  $home_dir = $id == 'root' ? { true => '/root', default => "/home/${id}" }
  $script_file = "${home_dir}/oh-my-zsh-install.zsh"
  $zshrc_file = "${home_dir}/.zshrc"
  package{['curl', 'zsh', 'git']:
    ensure => present,
  }

  -> file{$script_file:
    source         => 'https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh',
    owner          => $id,
    group          => $id,
    mode           => '0555',
    checksum       => 'sha256',
    checksum_value => 'f37ddbb0a0c4dd0e9ce1dd77ad2961ce33dc6bd5ce15ea642bab74440d971476'
  }

  -> exec{'run zsh-install':
    path    => ['/usr/bin:/usr/local/bin:/bin:/opt/crossbelt/embedded/bin:/opt/crossbelt/bin'],
    command => "sh ${script_file} --unattended",
    creates => "${home_dir}/.oh-my-zsh"
  }

  -> ini_setting { 'plugins':
    ensure  => present,
    path    => $zshrc_file,
    setting => 'plugins',
    value   => "(${plugins.join(' ')})",
  }
}
