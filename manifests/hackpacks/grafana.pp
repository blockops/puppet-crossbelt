class crossbelt::hackpacks::grafana(
  Enum[present, absent] $ensure = present,
) {
    class { 'grafana':
      install_method => 'repo',
      plugins        => {
        grafana-simple-json-datasource => { ensure => present }
      },
      cfg            => {
        app_mode => 'production',
        server   => {
          http_port     => 8080,
        },
        database => {
          type     => 'sqlite3',
          host     => '127.0.0.1:3306',
          name     => 'grafana',
          user     => 'root',
          password => '',
        },
        users    => {
          allow_sign_up => false,
        },
      },
    }

}


