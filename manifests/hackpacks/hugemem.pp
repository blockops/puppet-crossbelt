class crossbelt::hackpacks::hugemem(
  Enum[present, absent] $ensure = present,
) {

  file{'/etc/security/limits.d/60-memlock.conf':
      ensure  => $ensure,
      content => "*    - memlock 262144\nroot - memlock 262144"
  }

  file{'/etc/sysctl.d/60-hugepages.conf':
      ensure  => $ensure,
      content => 'vm.nr_hugepages=128'
  }
}