class crossbelt::hackpacks::collectd_plugins(
  Enum[present, absent] $ensure = present,
) {
  include crossbelt::hackpacks::collectd
  class { '::collectd':
    purge           => true,
    recurse         => true,
    purge_config    => true,
    minimum_version => '5.4',
  }
  class { 'collectd::plugin::cpu':
    reportbystate    => true,
    reportbycpu      => true,
    valuespercentage => true,
  }
  class {'collectd::plugin::uptime':
  }


  class { 'collectd::plugin::cuda':
  }
  class { 'collectd::plugin::disk':
    disks          => ['/^dm/'],
    ignoreselected => true,
    udevnameattr   => 'DM_NAME',
  }
  class { 'collectd::plugin::memory':
  }
  class { 'collectd::plugin::df':
    devices        => ['proc','sysfs'],
    mountpoints    => ['/'],
    fstypes        => ['nfs','tmpfs','autofs','gpfs','proc','devpts'],
    ignoreselected => true,
  }
  class { 'collectd::plugin::processes':
    processes       => ['process1', 'process2'],
    process_matches => [
      { name => 'process-all', regex => 'process.*' }
    ],
  }
  class {'collectd::plugin::sensors':
    sensors        => ['sensors-coretemp-isa-0000/temperature-temp2', 'sensors-coretemp-isa-0000/temperature-temp3'],
    ignoreselected => false,
  }
  class { 'collectd::plugin::tcpconns':
    localports  => ['25', '12026'],
    remoteports => ['25'],
    listening   => false,
  }
  class { 'collectd::plugin::threshold':
    hosts   => [
      {
        name    => 'example.com',
        plugins => [
          {
            name  => 'load',
            types => [
              {
                name        => 'load',
                data_source => 'shortterm',
                warning_max => $facts.dig('processors', 'count') * 1.2,
                failure_max => $facts.dig('processors', 'count') * 1.9,
              },
              {
                name        => 'load',
                data_source => 'midterm',
                warning_max => $facts.dig('processors', 'count') * 1.1,
                failure_max => $facts.dig('processors', 'count') * 1.7,
              },
              {
                name        => 'load',
                data_source => 'longterm',
                warning_max => $facts.dig('processors', 'count'),
                failure_max => $facts.dig('processors', 'count') * 1.5,
              },
            ],
          },
        ],
      },
    ],
  }
}
