class crossbelt::hackpacks::disable_miner_schedule(
  String $miner_disable_command,
  Variant[String, Array] $minute,
  Variant[String, Array] $hour,
  Variant[String, Array] $month    = '*',
  Variant[String, Array] $weekday  = '*',
  Variant[String, Array] $monthday = '*',
  Enum[present, absent] $ensure = present,
) {

  cron { 'disable_mining':
      ensure      => $ensure,
      command     => $miner_disable_command,
      environment => ['PATH=/bin:/usr/bin:/usr/sbin:/opt/crossbelt/bin:/opt/ethos/bin'],
      user        => 'root',
      hour        => $hour,
      minute      => $minute,
      month       => $month,
      weekday     => $weekday,
      monthday    => $monthday,
  }
}
