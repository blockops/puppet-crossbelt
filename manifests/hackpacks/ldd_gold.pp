class crossbelt::hackpacks::ldd_gold(
  Enum[present, absent] $ensure = present,
) {
  file{'/usr/bin/ld':
    ensure => link,
    target => '/usr/bin/ld.gold'
  }
}