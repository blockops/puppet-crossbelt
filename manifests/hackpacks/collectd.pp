class crossbelt::hackpacks::collectd(
  Enum[present, absent] $ensure = present,
) {
  class { '::collectd':
    purge           => true,
    recurse         => true,
    purge_config    => true,
    minimum_version => '5.4',
  }
  class { 'collectd::plugin::rrdtool':
    datadir          => '/var/lib/collectd/rrd',
    createfilesasync => false,
    rrarows          => 1200,
    rratimespan      => [3600, 86400, 604800, 2678400, 31622400],
    xff              => 0.1,
    cacheflush       => 900,
    cachetimeout     => 120,
    writespersecond  => 50
  }
}