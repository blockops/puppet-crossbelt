class crossbelt::hackpacks::pp_feature_mask(
  Enum[present, absent] $ensure = present,
) {
  if $::facts['gpu_type'] == 'amdgpu' {
    ini_setting { 'amdgpu.ppfeaturemask':
      ensure  => 'present',
      setting => 'GRUB_CMDLINE_LINUX_DEFAULT',
      path    => '/etc/default/grub',
      value   => '"quiet splash amdgpu.ppfeaturemask=0xffffffff"',
      notify  => Exec['update-grub']
    }
    exec{'update-grub':
      command     => '/usr/sbin/update-grub',
      refreshonly => true,
    }
  }
}