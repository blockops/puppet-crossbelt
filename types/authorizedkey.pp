type Crossbelt::Authorizedkey = Struct[
  {
    alias => Optional[String],
    key => String,
    type => Optional[Enum['ssh-rsa']],
    ensure => Optional[Enum[present, absent]],
    user   => String
  }
]
