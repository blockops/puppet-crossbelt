require 'digest'
require 'net/http'
require 'json'
require 'facter'

Facter.add(:site_id) do
  setcode do
    web_cache = '/var/run/tmpfiles.d/site_id'
    url = 'http://ip-api.com/json'
    # if older than one day do not use cache
    if File.readable?(web_cache) && (Time.now.to_i - File.mtime('/var/run/tmpfiles.d').to_i) < 86_400
      File.read(web_cache)
    else
      uri = URI.parse(url)
      response = Net::HTTP.get_response(uri)
      if response.code.eql?('200')
        location_data = JSON.parse(response.body, :symbolize_names => true)
        data = Digest::SHA512.hexdigest("#{location_data[:lat]},#{location_data[:lon]}").slice(0, 10)
        File.write(web_cache, data) if File.writable?(web_cache)
        data
      end
    end
  end
end
