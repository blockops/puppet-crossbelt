crossbelt_gem_path = Dir.glob('/opt/crossbelt/embedded/lib/ruby/gems').first
# I don't think we should have to load all the gem paths here but for now this seems to work

Facter.add(:location) do
  confine kernel: 'Linux'
  confine { !crossbelt_gem_path.nil? && File.exist?(crossbelt_gem_path) }
    setcode do
      begin
        Dir.glob(File.join(crossbelt_gem_path, 'gems', '*', 'lib')).each do |path|
          $LOAD_PATH << path unless $LOAD_PATH.include?(path)
        end
        require 'crossbelt'
        require 'crossbelt/location'
        { 'timezone' => Crossbelt::Location.timezone }
      rescue LoadError
        Facter.debug('Crossbelt library not installed')
      end
    end 
end
