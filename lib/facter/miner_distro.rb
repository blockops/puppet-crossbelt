Facter.add(:miner_distro) do
  confine kernel: 'Linux'

  setcode do
    match = Facter.value(:kernelrelease).match(/-(\D+)\d*\Z/)
    miner_distro = match.nil? ? 'unsupported' : match[1]
    miner_distro_version = case miner_distro
                           when 'ethos'
                             File.read('/opt/ethos/etc/version').chomp
                           when 'mmp'  
                            _, version = File.read('/opt/mmp/etc/conf.d/mmp-release').chomp.split('=') if File.exist?('/opt/mmp/etc/conf.d/mmp-release')
                            lsb_release = File.read('/opt/mmp/etc/conf.d/lsb-release').chomp.split("\n").map {|i| i.split('=') }.to_h if File.exist?('/opt/mmp/etc/conf.d/lsb-release')
                            version
                           else
                             'unsupported'
                           end
    {
      'name' => miner_distro,
      'version' => miner_distro_version,
    }
  end
end
