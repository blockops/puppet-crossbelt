Facter.add(:mining_profile_local) do
  confine kernel: 'Linux'

  setcode do
    config = Facter.value(:crossbelt_config) || {}
    config['mining_profile'] || 'default'
  end
end
