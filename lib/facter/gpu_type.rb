crossbelt_gem_path = Dir.glob('/opt/crossbelt/embedded/lib/ruby/gems/*').first

Facter.add(:gpu_type) do
  confine kernel: 'Linux'
  confine { !crossbelt_gem_path.nil? && File.exist?(crossbelt_gem_path) }
  setcode do
    Dir.glob(File.join(crossbelt_gem_path, 'gems', '*', 'lib')).each do |path|
      $LOAD_PATH << path unless $LOAD_PATH.include?(path)
    end
    begin
      require 'crossbelt/rig'
      Crossbelt::Rig.instance.type
    rescue LoadError
      Facter.debug('Crossbelt library not installed')
    end
  end
end
