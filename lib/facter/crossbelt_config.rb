crossbelt_gem_path = Dir.glob('/opt/crossbelt/embedded/lib/ruby/gems/*').first
require 'yaml'

Facter.add(:crossbelt_config) do
  confine kernel: 'Linux'
  confine { !crossbelt_gem_path.nil? && File.exist?(crossbelt_gem_path) }
  setcode do
    begin
      Dir.glob(File.join(crossbelt_gem_path, 'gems', '*', 'lib')).each do |path|
        $LOAD_PATH << path unless $LOAD_PATH.include?(path)
      end
      require 'crossbelt/config'
      YAML.load_file(Crossbelt::Config.global_config_file) if File.exist?(Crossbelt::Config.global_config_file)
    rescue LoadError
      Facter.debug('Crossbelt library not installed')
    end
  end
end
