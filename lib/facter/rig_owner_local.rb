Facter.add(:rig_owner_local) do
  confine kernel: 'Linux'

  setcode do
    config = Facter.value(:crossbelt_config) || {}
    config['rig_owner'] || 'default'
  end
end
