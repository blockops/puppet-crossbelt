crossbelt_gem_path = Dir.glob('/opt/crossbelt/embedded/lib/ruby/gems/*').first

Facter.add(:compute_units) do
  confine kernel: 'Linux'
  confine { !crossbelt_gem_path.nil? && File.exist?(crossbelt_gem_path) }

  setcode do
    begin
      Dir.glob(File.join(crossbelt_gem_path, 'gems', '*', 'lib')).each do |path|
        $LOAD_PATH << path unless $LOAD_PATH.include?(path)
      end
      require 'crossbelt/compute_units'
      data = Crossbelt::ComputeUnits.find_all.map do |cu|
        cu.hardware_info
      end
      # easily removes symbols by this dumb conversion
      JSON.parse(data.to_json)
    rescue LoadError
      Facter.debug('Crossbelt library not installed, unable to find compute units')
    end
  end
end
